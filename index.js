let todolists = [];

const addTodo = (todo) => {
	todolists.push(todo);
	console.log("todo added!");
	console.log("Current todolists:  \n", todolists);
};

const deleteTodo = (id) => {
	const updated = todolists.filter((todolists) => {
		return todolists.id !== id;
	});

	todolists = updated;
	console.log("todo deleted! \n Currrent todolists : \n", todolists);
};

const updateTodo = (id, newValue) => {
	todolists.forEach((todolist) => {
		if (todolist.id == id) {
			todolist.todo = newValue;
		}
	});
	console.log("Todo updated! \n Currrent todolists : \n", todolists);
};

const doingTodo = (todo) => {
	todolists.forEach((todolists) => {
		if (todolists.id == todo) {
			if (todolists.status == "waiting") {
				todolists.status = "on going";
			} else if (todolists.status == "on going") {
				todolists.status = "done";
			} else {
				todolists.status = "waiting";
			}
		}
	});

	console.log("Doing Todo! \n Currrent todolists : \n", todolists);
};


addTodo({
	id: 1,
	todo: "belajar javascript",
	status: "waiting",
});

addTodo({
	id: 2,
	todo: "latihan dance",
	status: "on going",
});

updateTodo(1, "kerjakan tugas");

doingTodo(1);

deleteTodo(1);
